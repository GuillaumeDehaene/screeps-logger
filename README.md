## A screeps-logger project

I'm trying to build a basic screeps-logger for the community.

Key features:
- automatic JSON.stringify on objects
- colored messages
- choose global verbose level
- choose message level
- build custom loggers with their own verbose, default message level, color

API:
logger.log(msg: str | Object, level?: int, color?: str)
logger.BuildLogger(tag="default", ops: Object)

Any ideas / feature-suggestions / code improvements very welcome on the screeps discord @dawn_of_victory.