var _ = require("lodash");

// build logging system
var logger = require("./logger");

// display an example
logger.test();

var log = logger.BuildLogger();
log("this custom logger follows options defined by config.default")

var error = logger.BuildLogger("critical");
error("this custom logger follows options defined by config.critical")

var cyan_log = logger.BuildLogger({"color": "#00FFFF"});
cyan_log("this custom logger follows options defined by default, but with color always set to cyan")


module.exports.loop = function () {
    // screeps code goes vroom
}