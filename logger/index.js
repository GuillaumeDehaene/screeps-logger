const logger = require("./logger");
module.exports = logger;

// also export console functions
if (!global.logger) {
    global.logger = {}
}
global.logger.set_verbose = logger.set_verbose;
global.logger.get_verbse = logger.get_verbose;