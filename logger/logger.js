const _ = require("lodash");    // not required in screeps


const config = require("./config");
const colors = require("./colors");


// util function: transform objects to string
const to_str = function(msg) {
    if (typeof(msg) == "string") {
        return msg
    } else {
        return JSON.stringify(msg, null, 2)
    }
}


// util function: build html flags from inputs
const build_flags = function(color) {
    // if color is null, use console default color
    if (color === "") {
        return ["", ""]
    }
    const pre = `<font color=${color}>`;
    const post = "</font>"
    return [pre, post]
}


// util function: clamp into to range
const clamp = function(x, a, b) {
    if (x <= a) {return a}
    if (x >= b) {return b}
    return x
}


// util function: clamp level then recover corresponding element from array
const color_from_level = function(level, colors) {
    if (Number.isInteger(level)) {
        return colors[clamp(level-1, 0, colors.length - 1)]
    }
    return ""
}


// key function: build a customized log function
module.exports.BuildLogger = function(tag="default", opts={}) {
    // supporting the (opts) signature
    if (typeof(tag) == 'object') {
        opts = tag;
        tag = "default";
    }
    // making sure that the tag exists makes everything simpler later
    if (!config[tag]) {
        config[tag] = {}
    }
    const custom_log = function (msg="", level=null, color=null) {
        // msg: str | Object
        // level: int; optional
        // color: str; optional

        // handle (msg, color) signature
        if (typeof(level) == String) {
            color = level;
            level = 1;
        }

        level = level || opts.level || config[tag].level || config["default"].level || 1;

        // compute verbose level
        const verbose = opts.verbose || config[tag].verbose || config["default"].verbose || 1;
        if (level < verbose) {
            return
        }

        msg = to_str(msg);

        // check if color is defined by tag or default
        color = color || opts.color || config[tag].color || config["default"].color || color;

        // if still undefined, use level and colors array
        if (color === null) {
            color = color_from_level(level, colors);
        }

        const [pre, post] = build_flags(color);
        console.log(pre + msg + post);
        return
    }
    return custom_log
}


// key function: slight extension of console.log
// uses the default configuration parameters
module.exports.log = module.exports.BuildLogger();


const error = module.exports.BuildLogger({"level": 5, "verbose": 5});


// key function: show current config as saved in the Memory
module.exports.show_config = function () {
    error("Current logger configuration:");
    error(config);
}


// key function: show current verbose levels
module.exports.get_verbose = function () {
    error("Current logger verbose levels:")
    // these are not tags
    const non_tag_keys = ["verbose", "msg_level", "color"];
    for (let key in config) {
        if (!non_tag_keys.includes(key)) {
            if (typeof(config[key].verbose) == "undefined") {
                error("  " + key + ": " + config["default"].verbose + "  (default)");
            } else {
                error("  " + key + ": " + config[key].verbose);
            }
        }
    }
}


// key function: set verbose level
module.exports.set_verbose = function(tag, verbose) {
    if (Number.isInteger(tag)) {
        verbose = tag;
        tag = "default";
    }

    config[tag].verbose = verbose;
}


// A small test of the library
module.exports.test = function() {
    const logger = module.exports;

    error();
    error("################");
    error("# logger demo #");
    error("##############");
    error();
    error("Configure logger using logger/config.js");
    logger.show_config();
    error();
    error("Current verbose levels can be recovered with logger.get_verbose() and set with logger.set_verbose([tag], level)");
    error("Both commands are accessible from within the console.");
    error("/!\\ Any console modifications won't survive a global reset /!\\");
    logger.get_verbose();
    error();
    error("Showing logging levels");
    let log = logger.BuildLogger({"verbose": 1});
    for (let i of _.range(1, 6)) {
        log(`  Level ${i}`, i);
    }
    error()
    error("Some of these levels will be hidden depending on the verbose level.");
    error("Let's repeat the preceding for loop with verbose = 3. This will hide levels 1 and 2:");
    log = logger.BuildLogger({"verbose": 3});
    for (let i of _.range(1, 6)) {
        log(`  Level ${i}`, i);
    }
    error()
    error("I hope you find these useful 👋");
}