module.exports = {
    "default": {
        "verbose": 3,
        "msg_level": 1,
        // "color": str
    },

    // for bot-breaking errors
    "critical": {
        "msg_level": 5,
        "verbose": 5,       // this verbose has priority over the global level: these messages will always be visible
    },

    // for all spawn related tasks
    "spawn": {
        "verbose": 1,  // I'm currently debugging this
    }
}